const { spawn } = require("child_process");

const processes = ["process1.js", "process2.js", "process3.js"];

const spawnProcess = (address, i) => new Promise((resolve => {
  let data = "";
  const process = spawn("node", [address]);

  process.stdout.on("data", chunk => data += chunk);

  process.on("close", () => {
    console.log(`Process #${i + 1} was ended`);
    resolve(data);
  });
}));

Promise
  .all(processes.map(spawnProcess))
  .then(data => console.log(
    `The result is: ` + data.reduce((a, b) => a + Number.parseInt(b), 0)
  ));
